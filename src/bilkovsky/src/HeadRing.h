#ifndef __HEADRING_H__
#define __HEADRING_H__

#include "DatList.h"

class THeadRing : public TDatList {
protected:
	PTDatLink pHead;     // ���������, pFirst - ����� ��������� �� pHead
public:
	THeadRing();
	~THeadRing();
	// ������� �������
	virtual void InsFirst(PTDatValue pVal = NULL); // ����� ���������
												   // �������� �������
	virtual void DelFirst(void);                 // ������� ������ �����
};


THeadRing::THeadRing() : TDatList()
{
	InsLast();
	pHead = pFirst;
	ListLen = 0;
	pStop = pHead;
	Reset();
	pFirst->SetNextLink(pFirst);
}

/*-------------------------------------------*/

THeadRing::~THeadRing()
{
	TDatList::~TDatList();
	DelLink(pHead);
	pHead = nullptr;
}

/*-------------------------------------------*/

void THeadRing::InsFirst(PTDatValue pVal) // �������� ����� ���������
{
	TDatList::InsFirst(pVal);
	if (RetCode == DataOK)
		pHead->SetNextLink(pFirst);
}

/*-------------------------------------------*/

void THeadRing::DelFirst(void) // ������� ������ �����
{
	TDatList::DelFirst();
	pHead->SetNextLink(pFirst);
}
#endif