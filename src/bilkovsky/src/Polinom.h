#ifndef __POLINOM_H__
#define __POLINOM_H__

#include "HeadRing.h"
#include "Monom.h"

#include <iostream>
using namespace std;

class TPolinom : public THeadRing {
public:
    TPolinom(int monoms[][2] = NULL, int km = 0); // �����������

    TPolinom(TPolinom &q);      // ����������� �����������
    PTMonom  GetMonom() { return (PTMonom)GetDatValue(); }
    TPolinom & operator+(TPolinom &q); // �������� ���������
    TPolinom & operator=(TPolinom &q); // ������������
    bool operator==(TPolinom &q); // ��������� ���������
    bool operator!=(TPolinom &q); // ��������� ���������
    double CalculatePoly(int x = 0, int y = 0, int z = 0);
    friend ostream& operator<<(ostream &os, TPolinom &q);// ������ ��������
};

TPolinom::TPolinom(int monoms[][2], int km)
{
    PTMonom pMonom = new TMonom(0, -1);
    pHead->SetDatValue(pMonom);
    for (int i = 0; i < km; i++)
    {
        pMonom = new TMonom(monoms[i][0], monoms[i][1]);
        InsLast(pMonom);
    }
}

TPolinom & TPolinom::operator+(TPolinom &q) // �������� ���������
{
    PTMonom pm, qm, tm;
    Reset();
    q.Reset();
    while (1)
    {
        pm = GetMonom();
        qm = q.GetMonom();
        if (pm->Index < qm->Index)
        {
            // ������� ������ pm ������ �������� ������ qm => ���������� ������ qm  � ������� p
            tm = new TMonom(qm->Coeff, qm->Index);
            InsCurrent(tm);
            q.GoNext();
        }
        else if (pm->Index > qm->Index)
            GoNext();
        else // ������� ������� �����
        {
            if (pm->Index == -1) // ������ �� ������ ���� �������������
                break;
            pm->Coeff += qm->Coeff;
            if (pm->Coeff != 0)
            {
                GoNext();
                q.GoNext();
            }
            else // �������� ������ � ������� �������������
            {
                DelCurrent();
                q.GoNext();
            }
        }
    }
    return *this;
}

TPolinom::TPolinom(TPolinom &q) // ����������� �����������
{
    PTMonom pMonom = new TMonom(0, -1);
    pHead->SetDatValue(pMonom);
    for (q.Reset(); !q.IsListEnded(); q.GoNext())
    {
        pMonom = q.GetMonom();
        InsLast(pMonom->GetCopy());
    }
    q.Reset();
}

TPolinom & TPolinom::operator=(TPolinom &q) // ������������
{
    if (this != &q)
    {
        PTMonom pMonom;
        DelList();
        for (q.Reset(); !q.IsListEnded(); q.GoNext())
        {
            pMonom = q.GetMonom();
            InsLast(pMonom->GetCopy());
        }
    }
    return *this;
}

ostream& operator<<(ostream &os, TPolinom &q)
{
    PTMonom pMonom;
    int cf, x, y, z;
    if (q.GetListLength() > 0)
    {
        for (q.Reset(); !q.IsListEnded(); q.GoNext())
        {
            pMonom = q.GetMonom();
            cf = pMonom->GetCoeff();
            x = pMonom->GetIndex();
            y = (x % 100) / 10;
            z = x % 10;
            x = x / 100;
            if (cf != 0)
            {
                if ((cf > 0) && (q.GetCurrentPos() != 0))
                    os << "+" << cf;
                else
                    os << cf;
                if (x > 0)
                    os << "x^" << x;
                if (y > 0)
                    os << "y^" << y;
                if (z > 0)
                    os << "z^" << z;
                os << " ";
            }
        }
    }
    else
        os << 0;
    q.Reset();
    return os;
}

bool TPolinom::operator==(TPolinom &q) // ��������� ���������
{
    if (GetListLength() != q.GetListLength())
        return false;
    else
    {
        PTMonom Mon1, Mon2;
        Reset(); q.Reset();
        while (!IsListEnded())
        {
            Mon1 = GetMonom();
            Mon2 = GetMonom();
            if (*Mon1 == *Mon2)
            {
                GoNext(); q.GoNext();
            }
            else
                return false;
        }
        return true;
    }
}

bool TPolinom::operator!=(TPolinom &q) // ��������� ���������
{
    return !(*this == q);
}

double TPolinom::CalculatePoly(int x, int y, int z)
{
    double res = 0;
    PTMonom mon;
    int indx, indy, indz;
    if (ListLen)
    {
        for (Reset(); !IsListEnded(); GoNext())
        {
            mon = GetMonom();
            indx = mon->Index / 100;
            indy = (mon->Index % 100) / 10;
            indz = mon->Index % 10;
            res += mon->Coeff*pow(x, indx)*pow(y, indy)*pow(z, indz);
        }
    }
    return res;
}
#endif